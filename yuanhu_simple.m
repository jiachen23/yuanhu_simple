clc
clear

%% 得取用户输入
choice = input('1.自动 2.自定义: ');

if choice == 1
    Xo = 0.1;
    Yo = 0.1;
    Xi = 0.11;
    Yi = -0.11;
    d_theta_total = -100;
    a = 10;
    a2 = 2;
    Vi = 0.1;
    Vmax = 1;
    T = 0.001;
    delta = 1e-5;
else
    Xo = input('Xo: ');
    Yo = input('Yo: ');
    Xi = input('Xi: ');
    Yi = input('Yi: ');
    d_theta_total = input('d_theta_total: ');
    a = input('a: ');
    a2 = input('a2: ');
    Vi = input('Vi: ');
    Vmax = input('Vmax: ');
    T = input('T: ');
    delta = input('delta (m): ');
end

%% 检查组合是否合法
if Vmax*T/delta > 640
    disp('组合不合法。Vmax*T/delta 必须小于640。');
    return
end

%% 处理角度
d_theta_total = d_theta_total / 180 * pi;

%% 处理轨迹方向
if d_theta_total < 0
    sign = -1;
else
    sign = 1;
end

%% 处理起始点
R = sqrt((Xi - Xo)^2 + (Yi - Yo)^2);
Xin = Xi - Xo;
Yin = Yi - Yo;

%% 处理终点角度
if Xin > 0
    theta_i = atan(Yin/Xin);
else
    theta_i = atan(Yin/Xin) + pi;
end
theta_s = theta_i;
theta_e_total = theta_s + d_theta_total;

%% 画基准轨迹
if sign == 1
    alpha = theta_i:pi/20000:theta_e_total;
else
    alpha = theta_e_total:pi/20000:theta_i;
end
xx = R*cos(alpha) + Xo;
yy = R*sin(alpha) + Yo;
subplot(2, 2, [1, 3]);
cla
plot(xx, yy, 'b:');
hold on
plot(Xo, Yo, 'bo');
hold on
Xe_ideal = R * cos(theta_e_total) + Xo;
Ye_ideal = R * sin(theta_e_total) + Yo;
plot(Xe_ideal, Ye_ideal, 'go');
xlabel('X (m)');
ylabel('Y (m)');
legend('计划轨迹', '原点', '终点');
axis equal;

%% 清除之前的误差图和速度图
subplot(2, 2, 2);
cla
title('速度');
xlabel('时间 (s)');
ylabel('速度 (m/s)');
hold on
subplot(2, 2, 4);
cla
title('半径误差');
xlabel('时间 (ms)');
ylabel('误差 (%)');
hold on

%% 换算最大速度到最大角速度
w_max = Vmax / R;

%% 换算起始速度到起始角速度
w_i = Vi / R;

%% 主循环
phase = 1;
w_s = w_i;
theta_s = theta_i;
Xs = Xi;
Ys = Yi;
i = 1;
while ~(w_s * R < 0.5 * a2 * T && (phase == 3 || phase == 4))
    % 判断是否该减速
    t = w_s / (a2/R);
    if phase < 3 && a2 < a
        theta_brake = theta_e_total - sign * (0.5 * (a2/R) * t^2) - 0.5 * a/a2 * sign * (w_s*T);
    else
        theta_brake = theta_e_total - sign * (0.5 * (a2/R) * t^2) - sign * (w_s*T);
    end    
    % 判断是在哪个阶段
    if phase == 1 && w_s + (a/R)*T > w_max, phase = 2, end
    if phase ~= 3 && (sign * (theta_brake - theta_s) <= 0), phase = 3, end
    if phase == 3 && sign * (theta_brake - theta_s) > 0, phase = 4, end
    
    % 判断终角速度
    switch phase
        case 1
            w_e = w_s + (a/R)*T;
        case 2
            w_e = w_max;
        case 3
            w_e = w_s - (a2/R)*T;
            if abs(w_e) < 1e-14 && sign * (theta_s + sign * (w_s +  w_e) * T/2 - theta_e_total) <= 0
                w_e = w_s;
            end
        case 4
            w_e = w_s;
    end
    
    % 算取角度变化
    d_theta = (w_s + w_e) * T / 2;
    
    % 算取终角度
    theta_e = theta_s + sign * d_theta;
    
    % 得到下一个目标点
    Xe = R * cos(theta_e) + Xo;
    Ye = R * sin(theta_e) + Yo;
    
    % 换成协议格式信息
    [xPSC, xARR, xAxis, xDIR, xS] = proto_value(Xs, Xe, delta, '00');
    [yPSC, yARR, yAxis, yDIR, yS] = proto_value(Ys, Ye, delta, '01');
    
    % 计算真实终点
    if xDIR == 1
        xsign = 1;
    else
        xsign = -1;
    end
    if yDIR == 1
        ysign = 1;
    else
        ysign = -1;
    end
    
    Xe = Xs + xsign * delta * xS;
    Ye = Ys + ysign * delta * yS;
    
    % 画图
    subplot(2, 2, [1, 3]);
    plot([Xs, Xe],[Ys, Ye], 'r-*');
    subplot(2, 2, 2);
    plot([i-1, i]*T, [w_s, w_e]*R, 'b-*');
    hold on
    subplot(2, 2, 4);
    R_actual = sqrt((Xe-Xo)^2 + (Ye-Yo)^2);
    R_error(i) = (R_actual - R) / R * 100;
    if i > 1
        plot([i-1, i], [R_error(i-1), R_error(i)], 'b-');
        hold on
    end
    i = i + 1;
    
    % 设置终角速度与终角度为下一循环的起角速度与起角度
    w_s = w_e;
    theta_s = theta_e;
    Xs = Xe;
    Ys = Ye;
        
    % 画图
    drawnow;
end

%% 显示总结
disp('速度误差 (m/s):');
disp(w_s*R);
disp('终点误差 (m):');
disp(sqrt((Xe-Xe_ideal)^2 + (Ye-Ye_ideal)^2));
disp(Xe-Xe_ideal);
disp(Ye-Ye_ideal);
hold off
disp('终点误差 (脉冲):');
disp(sqrt((Xe-Xe_ideal)^2 + (Ye-Ye_ideal)^2) / delta);
disp((Xe-Xe_ideal) / delta);
disp((Ye-Ye_ideal) / delta);





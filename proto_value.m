function [PSC, ARR, AXIS, DIR, S] = proto_value( Xs, Xe, delta, Axis )
%PROTO_VALUE 协议函数
%   按照每微妙的起点终点, 计算并返回协议化的数据
    pulse = 108e3;
    dx = Xe - Xs;   
    
    dxn = abs(dx);
    x_pulse_num = floor(dxn/delta);
    %% 查找x的分频值PSC和重载值ARR
    if x_pulse_num == 0
        PSC = 0;
        ARR = 0;
    else
        x_factor = pulse / x_pulse_num;
        i_psc = 0;
        while x_factor > 100
            x_factor = x_factor / 2;
            i_psc = i_psc + 1;
        end
        PSC = 2^i_psc;
        ARR = floor(x_factor);
    end
    
    %% 设置轴AXIS
    AXIS = Axis;
    
    %% 设置方向DIR
    if dx >= 0
        DIR = 1;
    else
        DIR = 0;
    end
    
    %% 设置脉冲数Ｓ
    if x_pulse_num == 0
        S = 0;
    else
        S = floor(floor(pulse/PSC)/ARR);
    end
end